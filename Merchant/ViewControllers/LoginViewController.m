//
//  LoginViewController.m
//  Merchant
//
//  Created by SCIT on 9/6/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import "LoginViewController.h"
#import "Constants.h"
#import "MenuViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self manualStyles];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)manualStyles{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self setbtnLoginBackground];
}
- (void)setbtnLoginBackground
{
    [btnSignin.layer setMasksToBounds:YES];
    [btnSignin.layer setBorderWidth:1.0f];
    [btnSignin.layer setBorderColor:[SEPERATER_BROWN CGColor]];
    btnSignin.backgroundColor = BACKGROUND_BROWN;
    
    [emailTF setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
}
- (IBAction)btnSignInClicks:(id)sender {
    NSString * email = emailTF.text;
    NSString * password = passwordTF.text;
    
    [self validateValues:email :password];
}
-(void)validateValues:(NSString *)email :(NSString *)password{
    
    if ((email.length > 0)&&(password.length >0)) {
        if ([self validEmail:email]) {
            //sendDetails
            [self navigateView];
        }else{
            [self showAlert:NSLocalizedString(@"app_name", nil) message:NSLocalizedString(@"email_incorrct", nil)];
        }
    }else{
        [self showAlert:NSLocalizedString(@"app_name", nil) message:NSLocalizedString(@"empty_fields", nil)];
    }
}
-(void)navigateView{
    MenuViewController * meunview = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
    [self.navigationController pushViewController:meunview animated:YES];
}
#pragma mark -
#pragma mark Validations
-(BOOL)validEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
@end
