//
//  MenuViewController.m
//  Merchant
//
//  Created by SCIT on 9/6/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import "MenuViewController.h"
#import "QRScannerViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnScanClicks:(id)sender {
    QRScannerViewController * scanner = [self.storyboard instantiateViewControllerWithIdentifier:@"QRScannerViewController"];
    [self.navigationController pushViewController:scanner animated:YES];
}
- (IBAction)btnClaimIdClicks:(id)sender {
    
}


@end
