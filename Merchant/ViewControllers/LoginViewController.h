//
//  LoginViewController.h
//  Merchant
//
//  Created by SCIT on 9/6/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"

@interface LoginViewController : MasterViewController{
    
    __weak IBOutlet UIButton *btnSignin;
    
    
    __weak IBOutlet UITextField *emailTF;
    __weak IBOutlet UITextField *passwordTF;
    
}

@end
