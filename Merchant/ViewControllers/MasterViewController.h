//
//  MasterViewController.h
//  Simplefire
//
//  Created by SCIT on 6/20/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "MBProgressHUD.h"

@interface MasterViewController : UIViewController{
    MBProgressHUD * HUD;
}

-(void)showAlert :(NSString *)title message:(NSString *)msg;
-(void)inithud :(NSString *)message;
-(void)initDownloadHud;
- (void)hudHide;

@property (nonatomic, retain) UIPopoverPresentationController *profileListPickerPopover;
@end
