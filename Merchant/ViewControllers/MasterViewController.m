//
//  MasterViewController.m
//  Simplefire
//
//  Created by SCIT on 6/20/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import "MasterViewController.h"
#import "MBProgressHUD.h"

@interface MasterViewController ()

@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)appleyCommonStyles{
    
    //Image View
}


-(void)showAlert :(NSString *)title message:(NSString *)msg{
    
    NSString * message =msg;
    if (message == nil)message = NSLocalizedString(@"unrecognized_respond", nil);
    
    UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:title
                                                                               message: message
                                                                        preferredStyle:UIAlertControllerStyleAlert                   ];
  
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [myAlertController dismissViewControllerAnimated:YES completion:nil];
                             
                         }];

    [myAlertController addAction: ok];
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:myAlertController animated:YES completion:nil];
    });
}

#pragma Mark -
#pragma ProgresView
-(void)inithud :(NSString *)message{
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.labelText = nil;
    HUD.detailsLabelText = nil;
    HUD.color = [UIColor clearColor];
    [self.navigationController.view addSubview:HUD];
    
//    CGRect frame = CGRectMake(0, 0, 100, 100);
    
//    LoadingView *loadingView = [[LoadingView alloc] initWithFrame:frame];
//    loadingView.backgroundColor = [UIColor clearColor];
//    HUD.dimBackground = YES;
//    HUD.customView = loadingView;
//    [HUD show:YES];
}

-(void)initDownloadHud{
    
//    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
//    HUD.mode = MBProgressHUDModeCustomView;
//    HUD.labelText = nil;
//    HUD.detailsLabelText = nil;
//    HUD.color = [UIColor whiteColor];
//    [self.navigationController.view addSubview:HUD];
//    
//    CGRect frame = CGRectMake(0, 0, 147, 145);
//    
//    uploadView *customView = [[uploadView alloc] initWithFrame:frame];
//    customView.backgroundColor = [UIColor whiteColor];
//    HUD.dimBackground = YES;
//    HUD.customView = customView;
//    [HUD show:YES];
}

- (void)hudHide{
    [HUD removeFromSuperview];
    [HUD hide:YES];
    HUD = nil;
}

-(void)didClicksLogout{
    NSLog(@"logoutMaster");
}
@end
