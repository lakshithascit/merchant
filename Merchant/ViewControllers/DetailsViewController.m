//
//  DetailsViewController.m
//  Merchant
//
//  Created by SCIT on 9/6/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import "DetailsViewController.h"
#import "Constants.h"

@interface DetailsViewController ()

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self navigationStyles];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    NSLog(@"qrcode %@",_inquaryNumber);
}
-(void)navigationStyles{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    self.navigationController.view.backgroundColor = BACKGROUND_BROWN;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.backgroundColor = BACKGROUND_BROWN;
}
@end
