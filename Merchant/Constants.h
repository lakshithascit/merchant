//
//  Constants.h
//  Customer
//
//  Created by Lakshitha on 26/11/2014.
//  Copyright (c) 2014 User. All rights reserved.
//  Final Vertion 2016/Aug/26
// IN QA version 1.0

#ifndef Cashier_Constants_h
#define Cashier_Constants_h


#define kOFFSET_FOR_KEYBOARD 120.0

//colors
#define BACKGROUND_BROWN [UIColor colorWithRed:173.0/255.0 green:66.0/255.0 blue:61.0/255.0 alpha:1.0]
#define SEPERATER_BROWN [UIColor colorWithRed:194.0/255.0 green:110.0/255.0 blue:103.0/255.0 alpha:1.0]


#define WEBSERVICEKEY                                  @"4D2hjVcw3EZ2lhiiSgaKkJEhb9OeEcAP"

//Appointments states

#define PENDING                                        1
#define READY                                          2
#define INPROGRESS                                     3
#define COMPLETE                                       4
#define DISABLED                                       5

//CommanCompanyName
#define ALLCOMPANIES                                   @"All Companies"

//LoginDetails
#define GRANDTYPE                                      @"password"
#define CLIENTID                                       @"client2id"
#define CLIENTSECRET                                   @"client2secret"
#define USERNAME                                       @"randy@yopmail.com"
#define PASSWORD                                       @"123456"

//buttontitle
#define DOWNLOAD                                      @"DOWNLOAD"
#define START                                         @"START"
#define CONTINUE                                      @"CONTINUE"
#define FINALIZE                                      @"FINALIZE"
#define SYNCAPPOINTMENT                               @"SYNC APPOINTMENT"


//RegX
#define REGEX_USER_NAME_LIMIT                          @"^.{1,100}$"
#define REGEX_ADDRESS_LIMIT                            @"^.{10,100}$"
#define REGEX_USER_NAME                                @"[A-Za-z0-9 ]*"
#define REGEX_EMAIL                                    @"[A-Z0-9a-z._%+-]{1,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT                           @"^.{1,20}$"
#define REGEX_PASSWORD                                 @"[A-Za-z0-9]{6,20}"
#define REGEX_PHONE_DEFAULT                            @"[0-9]{3}\\-[0-9]{3}\\-[0-9]{4}"

//New Server
//#define SERVICEURL                                      @"http://app.simplifya.com/api/"  //  WebService URL - Live

#define SERVICEURL                                      @"http://52.40.65.38/api/"  //  WebService URL - Live





#endif
