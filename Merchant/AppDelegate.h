//
//  AppDelegate.h
//  Merchant
//
//  Created by SCIT on 9/6/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

